#!/usr/bin/env python3
import pyglet

from . import noise

WORLD_RESOLUTION = 1000


def colour_scale(value, startcol, endcol, startval=0, endval=1):
    """Assign a value a colour based on a linear scale"""
    ratio = (value - startval) / (endval - startval)
    ratio = min((max(ratio, 0), 1))
    antiratio = 1 - ratio
    return (
        startcol[0] * antiratio + endcol[0] * ratio,
        startcol[1] * antiratio + endcol[1] * ratio,
        startcol[2] * antiratio + endcol[2] * ratio,
    )


def ground_colour(height, temp, humidity):
    if height - temp * 0.2 + humidity * 0.1 > 1:
        return (255, 255, 255)

    elif height + humidity * 0.4 > 0.6:
        return (120, 110, 100)

    else:
        fertility = humidity - abs(temp)
        return colour_scale(fertility, (120, 140, 55), (45, 100, 35), -2, 2)


class World:
    def __init__(self):
        self.height_map = noise.NoiseMap(0.9, 50)
        self.humidity_map = noise.NoiseMap(1.3, 20)
        self.temperature_map = noise.NoiseMap(1, 20)

        self.height_values = self.height_map.get_noise(
            WORLD_RESOLUTION, 10, (0, 0), y_magnitude=0.5
        )
        self.humidity_values = self.humidity_map.get_noise(WORLD_RESOLUTION, 10, (0, 0))
        self.temperature_values = self.temperature_map.get_noise(
            WORLD_RESOLUTION, 10, (0, 0)
        )

        self.vertex_list = self.get_vertex_list()

    def get_vertex_list(self):
        positions = []
        colours = []
        normals = []
        indices = []
        for x in range(WORLD_RESOLUTION):
            for z in range(WORLD_RESOLUTION):
                y = self.height_values[x][z]
                positions += [
                    10 * (x / WORLD_RESOLUTION - 0.5),
                    y,
                    10 * (z / WORLD_RESOLUTION - 0.5),
                ]
                colours += ground_colour(
                    y, self.temperature_values[x][z], self.humidity_values[x][z]
                )
                if x == 0:
                    hx = (self.height_values[x + 1][z] - y) / 2
                elif x == WORLD_RESOLUTION - 1:
                    hx = (y - self.height_values[x - 1][z]) / 2
                else:
                    hx = self.height_values[x + 1][z] - self.height_values[x - 1][z]

                if z == 0:
                    hz = (self.height_values[x][z + 1] - y) / 2
                elif z == WORLD_RESOLUTION - 1:
                    hz = (y - self.height_values[x][z - 1]) / 2
                else:
                    hz = self.height_values[x][z + 1] - self.height_values[x][z - 1]

                hy = 20 / WORLD_RESOLUTION
                mag = (hx ** 2 + hy ** 2 + hz ** 2) ** 0.5
                normals += [hx / mag, hy / mag, hz / mag]
                if z != WORLD_RESOLUTION - 1 and x != WORLD_RESOLUTION - 1:
                    indices += [
                        x * WORLD_RESOLUTION + z,
                        x * WORLD_RESOLUTION + z + 1,
                        (x + 1) * WORLD_RESOLUTION + z + 1,
                        (x + 1) * WORLD_RESOLUTION + z,
                    ]

        return pyglet.graphics.vertex_list_indexed(
            WORLD_RESOLUTION ** 2,
            tuple(indices),
            ("v3f/static", tuple(positions)),
            ("c3B/static", tuple([int(round(x)) for x in colours])),
            ("n3f/static", tuple(normals)),
        )
