#!/usr/bin/env python3
import numpy


def generate_map(
    arraysize,
    roughness,
    waves_per_octave=100,
    octaves=10,
    seed=None,
    y_magnitude=1,
    xz_magnitude=1,
    xz_offset=(0, 0),
):
    """Create random noise by adding rotated sine waves"""
    numpy.random.seed(seed)
    quality = waves_per_octave * octaves
    decrease = 2 ** (1 / waves_per_octave)
    y_decrease = decrease ** (-1 / roughness)
    angles = numpy.random.uniform(0, 2 * numpy.pi, size=quality)
    offsets = numpy.random.uniform(0, 2 * numpy.pi, size=quality)
    xz_scale = numpy.pi * xz_magnitude

    base = numpy.zeros((arraysize, arraysize))
    xx, zz = numpy.meshgrid(
        numpy.linspace(-xz_scale + xz_offset[0], xz_scale + xz_offset[0], arraysize),
        numpy.linspace(-xz_scale + xz_offset[1], xz_scale + xz_offset[1], arraysize),
        sparse=True,
    )

    for level in range(quality):
        level_decrease = decrease ** level
        level_y_decrease = y_decrease ** level
        distances = xx * numpy.cos(angles[level]) - zz * numpy.sin(angles[level])
        base += (
            numpy.sin(distances * level_decrease + offsets[level]) * level_y_decrease
        )

    base *= (
        y_magnitude * ((1 - y_decrease ** 2) / (1 - y_decrease ** (2 * quality))) ** 0.5
    )

    return base


class NoiseMap:
    def __init__(self, roughness, waves_per_octave, seed=None):
        if seed is None:
            numpy.random.seed(None)
            self.seed = numpy.random.randint(2 ** 32)
        else:
            self.seed = seed
        self.roughness = roughness
        self.waves_per_octave = waves_per_octave

    def get_noise(self, arraysize, octaves, xz_offset, y_magnitude=1, xz_magnitude=1):
        return generate_map(
            arraysize,
            self.roughness,
            seed=self.seed,
            waves_per_octave=self.waves_per_octave,
            octaves=octaves,
            xz_offset=xz_offset,
            y_magnitude=y_magnitude,
            xz_magnitude=xz_magnitude,
        )

    def __repr__(self):
        return f"{self.__class__.__name__}({self.roughness!r}, {self.waves_per_octave!r}, {self.seed!r})"
