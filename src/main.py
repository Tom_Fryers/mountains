import math

import pyglet

from . import world


def colour(r, g, b, a=1):
    return (pyglet.gl.GLfloat * 4)(r, g, b, a)


MOVE_SPEED = 1
ROT_SPEED = 0.1

config = pyglet.gl.Config(
    double_buffer=True, sample_buffers=1, samples=8, depth_size=24
)
window = pyglet.window.Window(
    width=1280,
    height=720,
    caption="Mountains",
    resizable=True,
    vsync=False,
    config=config,
)

pyglet.gl.glClearColor(0.7, 0.8, 0.9, 1)
pyglet.gl.glEnable(pyglet.gl.GL_DEPTH_TEST)
pyglet.gl.glEnable(pyglet.gl.GL_FOG)
pyglet.gl.glFogfv(pyglet.gl.GL_FOG_COLOR, colour(0.7, 0.8, 0.9))
pyglet.gl.glFogf(pyglet.gl.GL_FOG_DENSITY, 0.05)
pyglet.gl.glLightfv(pyglet.gl.GL_LIGHT0, pyglet.gl.GL_AMBIENT, colour(0, 0, 0))
pyglet.gl.glLightfv(pyglet.gl.GL_LIGHT0, pyglet.gl.GL_DIFFUSE, colour(1, 1, 1))
pyglet.gl.glLightfv(pyglet.gl.GL_LIGHT0, pyglet.gl.GL_SPECULAR, colour(1, 1, 1))
pyglet.gl.glColorMaterial(pyglet.gl.GL_FRONT_AND_BACK, pyglet.gl.GL_AMBIENT_AND_DIFFUSE)
pyglet.gl.glMaterialfv(
    pyglet.gl.GL_FRONT_AND_BACK, pyglet.gl.GL_SPECULAR, colour(0, 0, 0)
)
pyglet.gl.glMaterialfv(
    pyglet.gl.GL_FRONT_AND_BACK, pyglet.gl.GL_EMISSION, colour(0, 0, 0)
)
pyglet.gl.glMaterialf(pyglet.gl.GL_FRONT_AND_BACK, pyglet.gl.GL_SHININESS, 80)
pyglet.gl.glEnable(pyglet.gl.GL_LIGHTING)
pyglet.gl.glEnable(pyglet.gl.GL_LIGHT0)
pyglet.gl.glEnable(pyglet.gl.GL_COLOR_MATERIAL)
pyglet.gl.glLightModelf(pyglet.gl.GL_LIGHT_MODEL_LOCAL_VIEWER, True)

keys = pyglet.window.key.KeyStateHandler()
window.push_handlers(keys)
fps_display = pyglet.window.FPSDisplay(window=window)

terrain = world.World()
position = [0, 0, 0]
aim = [0, 0]

window.set_exclusive_mouse(True)


def sin(theta):
    return math.sin(math.radians(theta))


def cos(theta):
    return math.cos(math.radians(theta))


def move(keys, dt):
    motion = [0, 0]
    if keys[pyglet.window.key.W]:
        motion[1] += MOVE_SPEED * dt
    if keys[pyglet.window.key.S]:
        motion[1] -= MOVE_SPEED * dt
    if keys[pyglet.window.key.A]:
        motion[0] += MOVE_SPEED * dt
    if keys[pyglet.window.key.D]:
        motion[0] -= MOVE_SPEED * dt
    if keys[pyglet.window.key.SPACE]:
        position[1] += MOVE_SPEED * dt
    if keys[pyglet.window.key.LSHIFT]:
        position[1] -= MOVE_SPEED * dt
    position[0] += motion[0] * cos(aim[0]) + motion[1] * sin(aim[0])
    position[2] += motion[0] * -sin(aim[0]) + motion[1] * cos(aim[0])
    return pyglet.event.EVENT_HANDLED


@window.event
def on_resize(width, height):
    pyglet.gl.glViewport(0, 0, width, height)
    pyglet.gl.glMatrixMode(pyglet.gl.GL_PROJECTION)
    pyglet.gl.glLoadIdentity()
    pyglet.gl.gluPerspective(90, width / float(height), 0.1, 1000)
    pyglet.gl.glMatrixMode(pyglet.gl.GL_MODELVIEW)
    return pyglet.event.EVENT_HANDLED


@window.event
def on_mouse_motion(x, y, dx, dy):
    aim[0] -= dx * ROT_SPEED
    aim[1] += dy * ROT_SPEED
    if aim[1] < -90:
        aim[1] = -90
    elif aim[1] > 90:
        aim[1] = 90
    return pyglet.event.EVENT_HANDLED


@window.event
def on_draw():
    window.clear()
    pyglet.gl.glLoadIdentity()
    pyglet.gl.gluLookAt(
        *position,
        position[0] + math.sin(math.radians(aim[0])),
        position[1] + math.tan(math.radians(aim[1])),
        position[2] + math.cos(math.radians(aim[0])),
        0,
        1,
        0
    )
    pyglet.gl.glLightfv(
        pyglet.gl.GL_LIGHT0, pyglet.gl.GL_POSITION, (pyglet.gl.GLfloat * 4)(0, 1, 0, 0)
    )
    terrain.vertex_list.draw(pyglet.gl.GL_QUADS)
    fps_display.draw()
    return pyglet.event.EVENT_HANDLED


def update(dt):
    move(keys, dt)


def main():
    pyglet.clock.schedule(update)
    pyglet.app.run()


if __name__ == "__main__":
    main()
